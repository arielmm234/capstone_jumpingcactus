﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class InventoryButtonPress : MonoBehaviour {
    public bool isOpen = false;
    public EventSystem eventSystem;
    public Button CloseButton;
    public Button myButton;
    public Animator animator;
	// Use this for initialization
	void Start ()
    {
       
       
    }
	
    public void OpenInventory()
    {
        animator.SetBool("isOpen", true);
        isOpen = true;
    }

    public void CloseInventory()
    {
        animator.SetBool("isOpen", false);
        eventSystem.SetSelectedGameObject(null);
        isOpen = false;
        
    }

    // Update is called once per frame
    void Update ()
    {
       

        if (isOpen = true && Input.GetButtonDown("PadPress"))
        {
            myButton.Select();
            Debug.Log("OPENING");
            OpenInventory();
            Debug.Log("Open");
           
            return;
        }
        //    // Select the button
        //    Debug.Log("Button pressed");
        //EventSystem.current.SetSelectedGameObject(myButton.gameObject);
        //// Highlight the button
        //myButton.OnSelect(new BaseEventData(EventSystem.current));

    }
}
