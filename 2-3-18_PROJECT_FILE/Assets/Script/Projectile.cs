﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	public Rigidbody projecti;
	public Transform SpawnPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Circle"))
        {
            Rigidbody clone;
            clone = (Rigidbody)Instantiate(projecti, (SpawnPoint.position + new Vector3(0, 0, 0)), projecti.rotation);
            float point = -Camera.main.transform.localRotation.x * 40;
			if (point < 0) {
				point = 1.0f;
			}
				
            clone.velocity = SpawnPoint.TransformDirection(new Vector3(0, point, 10));

        }
    }
}
