﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RetrieveQuest : Quest {

	public Item ItemToRetrieve;

	[TextArea()]
	public string QuestSummary;

    public Inventory inventory;
	// Use this for initialization
	void Start ()
    {
		
	}

	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.CompareTag("Player") && Input.GetButton("Fire2"))
			{
			//if(Input.GetKeyDown(KeyCode.P))
			//{
			this.GetComponentInParent<QuestTrigger>().TriggerQuestDialogue();
				
			QuestManager.Instance.AddQuest(this);
			Debug.Log("Quest Added");
			//}
			}

		if(other.gameObject.CompareTag("Player") && Inventory.Instance.Items.Contains(ItemToRetrieve) && Input.GetButtonDown("Fire2"))
		{
			Debug.Log ("TAPOS NA");
			HasQuestFinished = true;
			Inventory.Instance.RemoveItem (ItemToRetrieve);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
//		if(Input.GetKeyDown(KeyCode.P))
//        {
//            QuestManager.Instance.AddQuest(this);
//			Debug.Log("Quest Added");
//        }
	}

    public override void OnStart()
    {
        if(HasQuestStarted)
        {
            // Start function for quest
			Debug.Log("Retrieve Quest Started");

        }
        //base.OnStart();
        
    }

    public override void OnUpdate()
    {
		


    }



    public override void OnFinished()
    {
        if (HasQuestFinished)
        {
			Debug.Log("Finished Quest");
        }
        //base.OnFinished();

    }
}
