using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour {

	public Text nameText;
	public Text dialogueText;
	public Animator animator;
    public Button Continue;
    public EventSystem eventstste;
	private Queue<string> sentences;

	// Use this for initialization
	void Start () {
		sentences = new Queue<string>();
	}

	public void StartDialogue (Dialogue dialogue)
	{
        Continue.Select();
        Cursor.lockState = CursorLockMode.None;
		animator.SetBool("IsOpen", true);

		nameText.text = dialogue.name;
		sentences.Clear();
        



        foreach (string sentence in dialogue.sentences)
		{
			sentences.Enqueue(sentence);
		}

		DisplayNextSentence();

		//DialoguePauseMenu.Paused ();
	}

	public void DisplayNextSentence ()
    {
        if (sentences.Count == 0)
		{
			EndDialogue();
			return;
		}
		string sentence = sentences.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(sentence));
	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

    public void Update()
    {
        DontDestroyOnLoad(gameObject);
        // Continue.Select();

    }

    public void EndDialogue()
	{
       
        animator.SetBool("IsOpen", false);
		//DialoguePauseMenu.Resume ();
		Cursor.lockState = CursorLockMode.Locked;

	}

}
