﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMeIDie : MonoBehaviour {

	public Dialogue dialogue;
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			//Destroy (this.gameObject);
			//this.GetComponent<DialogueTrigger>().TriggerDialogue();
			FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
			Debug.Log ("WORKING");
		}
}
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			//Destroy (this.gameObject);
			//this.GetComponent<DialogueTrigger>().TriggerDialogue();
			FindObjectOfType<DialogueManager>().EndDialogue();
			Debug.Log ("WORKING");
		}
	}
}
